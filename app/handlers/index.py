# Copyright 2014 Gregor Mazovec

from datetime import datetime, timedelta
from tornado import gen
from tornado.web import RequestHandler, Application, url, authenticated
from .base import BaseHandler
from ..data.user import User
from ..data.location import Location
from ..data.session import Session
from ..data.unique_user_stat import UniqueUserStat


class IndexHandler(BaseHandler):
    @gen.coroutine
    def get(self):
        session = self.application.session
        locations = None
        stats = None
        current_stats = yield Session.get_unique_stats(datetime.now() - timedelta(minutes=5), datetime.now())
        if session:
            location = yield Location.get_session_location(session.id)
            if location:
                locations = yield location.get_near_locations(5000)
            stats = yield UniqueUserStat.get_stats()
        self.render("home.html", near_locations=locations, unique_stats=stats, current_stats=current_stats)

