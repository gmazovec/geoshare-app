# Copyright 2014 Gregor Mazovec

from tornado import gen
from tornado.web import authenticated
from .base import BaseHandler
from ..data.user import User
from ..data.location import Location


class LocationHandler(BaseHandler):
    @authenticated
    @gen.coroutine
    def post(self):
        session = self.application.session
        latitude = self.get_body_argument("latitude")
        longitude = self.get_body_argument("longitude")
        if not (latitude and longitude):
            self.set_status(400)
        else:
            yield Location.set_session_location(session.id, latitude, longitude)
            location = yield Location.get_session_location(session.id)
            yield location.get_near_locations(700)
        self.finish()

