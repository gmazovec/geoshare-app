# Copyright 2014 Gregor Mazovec

from tornado import gen
from tornado.web import RequestHandler
from app.data.session import Session


class BaseHandler(RequestHandler):
    @gen.coroutine
    def prepare(self):
        sid = self.get_secure_cookie("sid")
        session = None
        if sid:
            session = yield Session.findSession(sid)
        self.application.session = session

    def get_current_user(self):
        session = self.application.session
        if session and session.active:
            return session

