# Copyright 2014 Gregor Mazovec

import logging
from tornado import gen
from tornado.web import authenticated
from .base import BaseHandler
from ..data.user import User
from ..data.location import Location
from ..data.session import Session

log = logging.getLogger(__name__)


class LoginHandler(BaseHandler):
    def get(self):
        self.render("login.html")


class SessionHandler(BaseHandler):
    @gen.coroutine
    def post(self):
        email_address = self.get_body_argument("email_address")
        location_lat = self.get_body_argument("location_lat")
        location_long = self.get_body_argument("location_long")
        if not email_address:
            self.send_status(400)
            self.write("400")
            self.finish()
            return
        user = yield User.findUser(email_address)
        if not user:
            user = yield User.createUser(email_address)
            log.info("USER CREATED", user.id)
        session = yield Session.createSession(user.id)
        if location_lat and location_long:
            Location.set_session_location(session.id, location_lat, location_long)
        self.set_status(201)
        self.set_secure_cookie("sid", session.sid, expires_days=365)
        self.redirect("/")


class CurrentSessionHandler(BaseHandler):
    @authenticated
    @gen.coroutine
    def post(self):
        method = self.get_body_argument("_method")
        if method == "DELETE":
            session = self.application.session
            yield session.close()
            self.clear_cookie("sid")
            self.redirect("/")
        else:
            self.set_status(405)

