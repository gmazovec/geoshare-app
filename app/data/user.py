# Copyright 2014 Gregor Mazovec

import logging
import datetime
from tornado import gen
from sql import *
from sql.aggregate import *
from sql.conditionals import *
from uuid import uuid1
from app.data.base import AsyncEntry

log = logging.getLogger(__name__)


def get_timestamp():
    return datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%m:%S")

class User(AsyncEntry):
    _attributes = ("id", "email_address")
    _source_name = "app_user"

    @gen.coroutine
    def createUser(email_address):
        user = User()
        user.email_address = email_address
        yield user.persist()
        return user

    @gen.coroutine
    def findUser(search_value):
        user = User()
        table = Table(User._source_name)
        select = table.select()
        if type(search_value) == int:
            select.where = table.id == search_value
        else:
            select.where = table.email_address == search_value
        query = tuple(select)
        log.debug(query[0], query[1])
        cursor = yield user.execute(query[0], query[1])
        data = cursor.fetchone()
        if data:
            return User(data)

    @gen.coroutine
    def getUserId(email_address):
        user = User()
        table = Table(User._source_name)
        select = table.select(table.id)
        select.where = table.email_address == email_address
        query = tuple(select)
        log.debug(query[0], query[1])
        cursor = yield user.execute(query[0], query[1])
        data = cursor.fetchone()
        if data:
            return data[0]

