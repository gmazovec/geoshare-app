# Copyright 2014 Gregor Mazovec

import logging
from datetime import datetime, timedelta
from tornado import gen
from sql import *
from sql.aggregate import *
from sql.conditionals import *
from uuid import uuid1
from app.data.base import AsyncEntry
from app.data.session import Session

log = logging.getLogger(__name__)


class Interval:
    _callback = None
    _loop = None
    _state = -1

    def __init__(self, loop, callback):
        self._callback = callback
        self._loop = loop

    def _run(self, interval):
        if self._state == 1:
            try:
                self._callback()
            except Exception as e:
                log.error("Error calling Interval._callback", e)
            self._loop.call_later(interval, self._run, interval)
        else:
            self._state = -1

    def start(self, delay=1, interval=15):
        if self._state == -1:
            self._state = 1
            self._loop.call_later(delay, self._run, interval)

    def stop(self):
        if self._state == 1:
            self._state = 0

def time_to_key(time):
    return time.strftime("%y%m%d%H")

def get_datetime(key):
    d = [int(key[i:i+2]) for i in range(0, len(key), 2)]
    return datetime(d[0] + 2000, d[1], d[2], d[3])

def get_start_key():
    time = datetime.now() - timedelta(days=1)
    return time_to_key(time)

def get_current_key():
    return time_to_key(datetime.now())

def get_next_key(key):
    time = get_datetime(key) + timedelta(hours=1)
    return time_to_key(time)


class UniqueUserStat(AsyncEntry):
    _attributes = ("id", "key", "value")
    _source_name = "unique_user_per_hour"
    _aggregator = None

    def get_key_label(self):
        return get_datetime(self.key).strftime("%Hh, %d.%m")

    @gen.coroutine
    def _getLastEntry():
        stat = UniqueUserStat()
        table = Table(UniqueUserStat._source_name)
        query = tuple(table.select(order_by=table.key.desc))
        sql = query[0] + " LIMIT 1"
        cursor = yield stat.execute(query[0], query[1])
        if cursor.rowcount > 0:
            data = cursor.fetchone()
            return UniqueUserStat(data)

    @gen.coroutine
    def _aggregate_hour_stats(key):
        timestamp = get_datetime(key)
        unique_users = yield Session.get_unique_stats(timestamp - timedelta(hours=1), timestamp)
        try:
            stat = UniqueUserStat()
            stat.key = key
            stat.value = unique_users
            yield stat.persist()
        except Exception as e:
            log.error("ERROR", e)


    @gen.coroutine
    def _aggregate():
        last_stat = yield UniqueUserStat._getLastEntry()
        current_key = get_current_key()
        if last_stat:
            last_key = last_stat.key
        else:
            last_key = get_start_key()
        while last_key <= current_key:
            last_key = get_next_key(last_key)
            yield UniqueUserStat._aggregate_hour_stats(last_key)

    def start_aggregation(loop):
        if not UniqueUserStat._aggregator:
            UniqueUserStat._aggregator = Interval(loop, UniqueUserStat._aggregate)
            UniqueUserStat._aggregator.start(1, 60)

    @gen.coroutine
    def get_stats():
        stat = UniqueUserStat()
        table = Table(UniqueUserStat._source_name)
        time = datetime.now() - timedelta(days=1)
        key = time_to_key(time)
        select = table.select()
        select.where = table.key >= key
        query = tuple(select)
        cursor = yield stat.execute(query[0], query[1])
        if cursor.rowcount > 0:
            data = []
            for i in range(0, cursor.rowcount):
                data.append(UniqueUserStat(cursor.fetchone()))
            return data

