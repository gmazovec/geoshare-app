# Copyright 2014 Gregor Mazovec

import logging
from tornado import gen
from sql import *
from sql.aggregate import *
from sql.conditionals import *
from uuid import uuid1
from app.data.base import AsyncEntry
from app.data.location import Location

log = logging.getLogger(__name__)


class Session(AsyncEntry):
    _attributes = (
        "id", "sid", "user_id", "active", "share_loc", "created_on"
    )
    _source_name = "app_session"

    @gen.coroutine
    def get_unique_stats(start_time, end_time):
        try:
            session = Session()
            table = Table(Session._source_name)
            join_table = Table(Location._source_name)
            join = Join(table, join_table)
            join.condition = join.right.session_id == table.id
            select = join.select(Count(table.id))
            select.where = (table.active == True) & ((table.created_on >= start_time) & (table.created_on < end_time) | (join.right.updated_on >= start_time) & (join.right.updated_on < end_time))
            select.group_by = table.user_id
            query = tuple(select)

            sql = query[0].replace("INNER", "RIGHT")
            log.debug(sql % query[1])
        except Exception as e:
            log.error("ERROR", e)
        cursor = yield session.execute(sql, query[1])
        return cursor.rowcount

    @gen.coroutine
    def createSession(user_id):
        session = Session()
        session.sid = str(uuid1())
        session.user_id = user_id
        session.active = True
        session.share_loc = True
        yield session.persist()
        return session

    @gen.coroutine
    def findSession(search_value):
        session = Session()
        table = Table(Session._source_name)
        select = table.select()
        if type(search_value) == int:
            select.where = table.id == search_value
        else:
            if type(search_value) == bytes:
                search_value = search_value.decode()
            select.where = table.sid == search_value
        query = tuple(select)
        cursor = yield session.execute(query[0], query[1])
        data = cursor.fetchone()
        return Session(data)

    @gen.coroutine
    def close(self):
        self.active = False
        yield self.persist()

