# Copyright 2014 Gregor Mazovec

from tornado import gen
from sql import *
from sql.aggregate import *
from sql.conditionals import *
from app.data.base import Entry, AsyncEntry


class NearLocation(Entry):
    _attributes = (
        "id", "session_id", "latitude", "longitude", "updated_on",
        "distance", "center_latitude", "center_longitude"
    )


class Location(AsyncEntry):
    _attributes = (
        "id", "session_id", "latitude", "longitude", "updated_on"
    )
    _source_name = "geolocation"

    @gen.coroutine
    def set_session_location(session_id, latitude, longitude):
        location = yield Location.get_session_location(session_id)
        if not location:
            location = Location()
            location.session_id = session_id
        location.latitude = latitude
        location.longitude = longitude
        yield location.persist()
        return True

    @gen.coroutine
    def get_session_location(session_id):
        location = Location()
        table = Table(location._source_name)
        select = table.select()
        select.where = table.session_id == session_id
        query = tuple(select)
        cursor = yield location.execute(query[0], query[1])
        data = cursor.fetchone()
        if data:
            return Location(data)

    @gen.coroutine
    def get_near_locations(self, radius):
        sql = """
            SELECT g.*, earth_distance(ll_to_earth(g.latitude, g.longitude), ll_to_earth(%s, %s)) AS distance
            FROM geolocation AS g
            LEFT JOIN app_session AS s ON g.session_id = s.id
            WHERE s.active = True
                AND earth_box(ll_to_earth(%s, %s), %s) @> ll_to_earth(g.latitude, g.longitude)
                AND earth_distance(ll_to_earth(g.latitude, g.longitude), ll_to_earth(%s, %s)) <= %s
                AND g.session_id <> %s
                AND current_timestamp - interval '15 minutes' < g.updated_on
            ORDER BY distance ASC
            LIMIT 50
            """
        lat = self.latitude
        lng = self.longitude
        values = (lat, lng, lat, lng, radius, lat, lng, radius, self.session_id)
        cursor = yield self.execute(sql, values)
        if cursor.rowcount > 0:
            locations = []
            data = cursor.fetchall()
            for item in data:
                location = NearLocation(item)
                location.center_latitude = self.latitude
                location.center_longitude = self.longitude
                locations.append(location)
            return locations

