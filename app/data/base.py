# Copyright 2014 Gregor Mazovec

import datetime
import logging
import sql
import momoko
import psycopg2
from tornado import gen

Op = gen.Callback
WaitOp = momoko.WaitOp
log = logging.getLogger(__name__)


def get_timestamp():
    return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")


def prepareData(application):

    conn = psycopg2.connect("dbname=geoshare_dev user=geoshare password=geoshare host=localhost")
    sql = (

        "CREATE EXTENSION IF NOT EXISTS cube",
        "CREATE EXTENSION IF NOT EXISTS earthdistance",

        "CREATE TABLE IF NOT EXISTS app_user ("
            "id SERIAL NOT NULL,"
            "email_address CHAR(50) NOT NULL,"
            "PRIMARY KEY (id),"
            "UNIQUE (email_address)"
        ")",

        "CREATE TABLE IF NOT EXISTS app_session ("
            "id SERIAL NOT NULL,"
            "sid CHAR(36) NOT NULL,"
            "user_id INTEGER NOT NULL,"
            "active BOOLEAN,"
            "share_loc BOOLEAN,"
            "created_on TIMESTAMP,"
            "PRIMARY KEY (id)"
        ")",

        "CREATE INDEX ON app_session (sid)",
        "CREATE INDEX ON app_session (user_id)",
        "CREATE INDEX ON app_session (created_on)",

        "CREATE TABLE IF NOT EXISTS geolocation ("
            "id SERIAL NOT NULL,"
            "session_id INTEGER NOT NULL,"
            "latitude DOUBLE PRECISION,"
            "longitude DOUBLE PRECISION,"
            "updated_on TIMESTAMP,"
            "PRIMARY KEY (id),"
            "UNIQUE (session_id)"
        ")",

        "CREATE INDEX ON geolocation (updated_on)",
        "CREATE INDEX on geolocation USING gist(ll_to_earth(latitude, longitude))",

        "CREATE TABLE IF NOT EXISTS unique_user_per_hour ("
            "id SERIAL NOT NULL,"
            "key CHAR(8),"
            "value INTEGER,"
            "PRIMARY KEY (id),"
            "UNIQUE(key)"
        ")",
    )

    cur = conn.cursor()
    for query in sql:
        result = cur.execute(query)
    conn.commit()
    cur.close()
    conn.close()

    log.info("Database prepared")
    application.db = momoko.Pool(
        dsn='dbname=geoshare_dev user=geoshare password=geoshare '
            'host=localhost port=5432',
        size=1
    )

    AsyncEntry.db = application.db


class Entry:
    _attributes = ()

    def __init__(self, data=None):
        if data:
            for attr, value in zip(self._attributes, data):
                if attr == None:
                    raise Exception("Invalid number or Entry attributes")
                self.__dict__[attr] = value
        else:
            for attr in self._attributes:
                self.__dict__[attr] = None

    def __setattr__(self, attr, value):
        if attr == "id":
            raise Exception("Attribute id is readonly")
        self.__dict__[attr] = value


class AsyncEntry:
    _attributes = ()
    db = None

    def __init__(self, data=None):
        if data:
            for attr, value in zip(self._attributes, data):
                if attr == None:
                    raise Exception("Invalid number or Entry attributes")
                self.__dict__[attr] = value
        else:
            for attr in self._attributes:
                if attr == "created_on" or attr == "updated_on":
                    self.__dict__[attr] = get_timestamp()
                else:
                    self.__dict__[attr] = None

    def __setattr__(self, attr, value):
        if attr == "id":
            raise Exception("Attribute id is readonly")
        self.__dict__[attr] = value

    @gen.coroutine
    def execute(self, sql, params=None):
        self.db.execute(sql, params, callback=(yield Op("q")))
        cursor = yield WaitOp("q")
        return cursor

    @gen.coroutine
    def __persist__(self, data):
        table = sql.Table(self._source_name)
        columns = []
        values = []
        for attr in self._attributes:
            value = self.__dict__[attr]
            if attr != "id" and value != None:
                if attr == "updated_on":
                    value = get_timestamp()
                    self.__dict__[attr] = value
                columns.append(sql.Column(table, attr))
                values.append(value)
        if self.id:
            id_col = sql.Column(table, "id")
            cond = (id_col == self.id)
            query = tuple(table.update(columns=columns, values=values, where=cond))
        else:
            query = list(table.insert(columns=columns, values=[values]))
            query[0] += " RETURNING id"
        log.info("Query: " + query[0] % query[1])
        cursor = yield self.execute(query[0], query[1])

        if not self.id:
            self.__dict__["id"] = cursor.fetchone()[0]

    @gen.coroutine
    def __unpersist__(self, id):
        yield self.execute("SELECT 2")

    @gen.coroutine
    def persist(self):
        yield self.__persist__(self.__dict__)

    @gen.coroutine 
    def unpersist(self):
        yield self.__persist__(self.__dict__["id"])

