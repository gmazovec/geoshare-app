
FROM ubuntu:14.04.1
MAINTAINER Gregor Mazovec

RUN apt-get update
RUN apt-get install -y python3 python3-pip python3-psycopg2
RUN apt-get install -y postgresql postgresql-contrib
RUN pip3 install tornado python-sql momoko

EXPOSE 8080

USER postgres

RUN /etc/init.d/postgresql start &&\ 
    psql --command "CREATE USER geoshare WITH SUPERUSER PASSWORD 'geoshare'" &&\ 
    psql --command "CREATE DATABASE geoshare_dev ENCODING 'UTF8' TEMPLATE template0" &&\ 
    psql --command "GRANT ALL PRIVILEGES ON DATABASE geoshare_dev TO geoshare"

USER root

CMD ["./run.sh"]
