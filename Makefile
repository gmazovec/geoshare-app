
IMAGE_NAME=gmazovec/geoshare-app
WD=/opt/geoshare-app
PORT_MAP=80:8080
APP_PATH=$(shell pwd)

all: build

build:
	docker build -t ${IMAGE_NAME} .

run:
	docker run -i -t -p ${PORT_MAP} -u root -v ${APP_PATH}:${WD} -w ${WD} ${IMAGE_NAME}

start:
	docker run -i -t -p ${PORT_MAP} -u root -v ${APP_PATH}:${WD} -w ${WD} ${IMAGE_NAME} bash

clean:
	docker rmi ${IMAGE_NAME}

