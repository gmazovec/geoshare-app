# Copyright 2014 Gregor Mazovec

import logging
from tornado import gen
from tornado.ioloop import IOLoop
from tornado.web import Application, url
from app.data.base import prepareData
from app.data.unique_user_stat import UniqueUserStat
from app.handlers import *

logging.basicConfig(level=logging.INFO)
logging.basicConfig(level=logging.DEBUG)


def make_application():
    urls = [
        url(r"/", IndexHandler),
        url(r"/login", LoginHandler),
        url(r"/session", SessionHandler),
        url(r"/session/@current", CurrentSessionHandler),
        url(r"/location", LocationHandler)
    ]
    settings = {
        "cookie_secret": "4r07|14U9;/5l#Z2s+Z&+:6/5_'S?<.4?m p#p9$+#/+W",
        "debug": True,
        "login_url": "/login",
        "static_path": "static",
        "template_path": "templates"
    }
    return Application(urls, **settings)

def main():
    loop = IOLoop.current()
    application = make_application()
    prepareData(application)
    application.listen(8080)
    UniqueUserStat.start_aggregation(loop)
    loop.start()


if __name__ == "__main__":
    main()

