// Copyright (C) 2014 Gregor Mazovec


function fetchGeolocation(cb) {
    if ('geolocation' in navigator) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var coords = position.coords;
            cb(null, coords);
        }, cb);
        return;
    }
    return new Error('Geolocation not supported');
}


function getUserLocation(cb) {
    var data = localStorage.getItem('current-location');
    
    if (data) {
        return cb(null, JSON.parse(data));
    }
    fetchGeolocation(function (err, coords) {
        var data = {};
        if (coords) {
            data = {latitude: coords.latitude, longitude: coords.longitude};
        }
        cb(err, data);
    });
}


function setUserLocation(data) {
    localStorage.setItem('current-location', JSON.stringify(data));
}


function syncUserLocation(coords) {
    var req = $.post('/location', coords);
    
    req.done(function (data, status, xhr) {
        console.log(data, status );
    });
}


getUserLocation(function (err, coords) {
    if (err) {
        alert(err.message);
    }
    if (coords && coords.latitude) {
        var latitude = parseFloat(coords.latitude).toFixed(7);
        var longitude = parseFloat(coords.longitude).toFixed(7);
        $('#geoloc').text('lat: ' + latitude + ', long: ' + longitude);
        $('#login-location-lat').val(coords.latitude);
        $('#login-location-long').val(coords.longitude);
        $('#login-btn').attr('disabled', false);
        syncUserLocation(coords);
    }
});

