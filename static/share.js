// Copyright (C) 2014 Gregor Mazovec


function updateLocation(coords, cb) {
    var data = {lat: coords.latitude, long: coords.longitude};
    var req = $.post('/location', data);

    req.done(function (data, status, xhr) {
        if (status !== 'success') {
            return cb(new Error('Location update error'));
        }
        cb(null);
    });
}


function shareLocation(cb) {
    var emailAddress = prompt('Enter your email address');

    if (!emailAddress) {
        return cb(new Error('Missing display name or email address'));
    }

    var data = {email_address: emailAddress};
    var req = $.post('/login', data);

    req.done(function (data, status, xhr) {
        if (status !== 'success') {
            return cb(new Error('Login error'));
        }
        getUserLocation(function (err, coords) {
            if (err) {
                return cb(err);
            }
            updateLocation(coords, cb);
        })
        cb(null);
    });
}
